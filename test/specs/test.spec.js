// Aigerim Nurmanova
describe("Test suite", ()=>{
    beforeEach(async()=>{
        await browser.url('https://pastebin.com/')
    })
    it('Check page title', async()=>{
        await expect(browser).toHaveTitle('Pastebin.com - #1 paste tool since 2002!')
    })

    it('Check paste text', async()=>{
        //set paste Text
        const pasteName = await $('#postform-text')
        await pasteName.setValue('Hello from WebDriver')
    })
    it('Check paste name', async()=>{
        //set paste Name
        const pasteText = await $('#postform-name')
        await pasteText.setValue('helloweb')
    })
    it('Check paste expiration to 10 minutes', async() => {
        // Wait for the page to load and the expiration dropdown to be clickable
        const  expirationDropdown = await $('#select2-postform-expiration-container');
        await expirationDropdown.click()
    
        // Wait for the options to be visible
        const expirationOptions = await $$('.select2-results__option');
        await expirationOptions[2].waitForDisplayed(); // Index 2 corresponds to "10 Minutes" option
    
        // Select "10 Minutes" option
        await expirationOptions[2].click();
            
        // Verify that the selected value is "10 Minutes"
        const selectedExpiration = expirationDropdown.getText();
        console.log(selectedExpiration)
    });

    it('Check paste click', async()=>{
        //click Button Paste
        const btn = await $('button.btn.-big')
        await btn.click()
    })
})